# Swastika-Web-Client [![CircleCI](https://circleci.com/gh/Swastika-IO/Swastika-Web-Front.svg?style=svg)](https://circleci.com/gh/Swastika-IO/Swastika-Web-Front)
----
### Introduction


### Development Setup
- Copy to local
- Run command: npm install

### Questions


### Issues


### Contribution


### Changelog


### Stay In Touch
https://twitter.com/Swastika_IO

### Front Structure
- main
  - header
  - nav
    - base-nav
    - sub-nav
  - dashboard
    - grid
      - block
  - footer


### License
