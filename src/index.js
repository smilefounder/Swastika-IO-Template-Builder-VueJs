import Vue from 'vue';
import Root from './app/root/root.vue';
import Config from './index.config';

import './index.scss';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      components: {
        default: Root
      }
    }
  ]
});

export default new Vue({
  el: '#root',
  router,
  data: () => {
    return {
      config: new Config(),
      context: null
    };
  },
  render: h => h('router-view')
});
